
<?php
// BMP085 default address.
const BMP085_I2CADDR           = 0x77;

// Operating Modes
const BMP085_ULTRALOWPOWER     = 0;
const BMP085_STANDARD          = 1;
const BMP085_HIGHRES           = 2;
const BMP085_ULTRAHIGHRES      = 3;

// BMP085 Registers
const BMP085_CAL_AC1           = 0xAA;  # R   Calibration data (16 bits)
const BMP085_CAL_AC2           = 0xAC;  # R   Calibration data (16 bits)
const BMP085_CAL_AC3           = 0xAE;  # R   Calibration data (16 bits)
const BMP085_CAL_AC4           = 0xB0;  # R   Calibration data (16 bits)
const BMP085_CAL_AC5           = 0xB2;  # R   Calibration data (16 bits)
const BMP085_CAL_AC6           = 0xB4;  # R   Calibration data (16 bits)
const BMP085_CAL_B1            = 0xB6;  # R   Calibration data (16 bits)
const BMP085_CAL_B2            = 0xB8;  # R   Calibration data (16 bits)
const BMP085_CAL_MB            = 0xBA;  # R   Calibration data (16 bits)
const BMP085_CAL_MC            = 0xBC;  # R   Calibration data (16 bits)
const BMP085_CAL_MD            = 0xBE;  # R   Calibration data (16 bits)
const BMP085_CONTROL           = 0xF4;
const BMP085_TEMPDATA          = 0xF6;
const BMP085_PRESSUREDATA      = 0xF6;

// Commands
const BMP085_READTEMPCMD       = 0x2E;
const BMP085_READPRESSURECMD   = 0x34;


class BMP085 {
    function __construct($mode=BMP085_STANDARD, $address=BMP085_I2CADDR, $i2c=None, $kwargs) {
        #$this->_logger = logging.getLogger('Adafruit_BMP.BMP085')
        # Check that mode is valid.
        #if (mode not in [BMP085_ULTRALOWPOWER, BMP085_STANDARD, BMP085_HIGHRES, BMP085_ULTRAHIGHRES]){
        #    raise ValueError('Unexpected mode value {0}.  Set mode to one of BMP085_ULTRALOWPOWER, BMP085_STANDARD, BMP085_HIGHRES, or BMP085_ULTRAHIGHRES'.format(mode))
        #}
        #$this->_mode = mode
        ## Create I2C device.
        # if i2c is None:
        #    import Adafruit_GPIO.I2C as I2C
        #    i2c = I2C
        #$this->_device = i2c.get_i2c_device(address, $kwargs)
    # Load calibration values.
    $this->loadCalibration();
    }




    function loadCalibration(){
        $this->cal_AC1 = $this->_device.readS16BE(BMP085_CAL_AC1);   # INT16
        $this->cal_AC2 = $this->_device.readS16BE(BMP085_CAL_AC2);   # INT16
        $this->cal_AC3 = $this->_device.readS16BE(BMP085_CAL_AC3);   # INT16
        $this->cal_AC4 = $this->_device.readU16BE(BMP085_CAL_AC4);   # UINT16
        $this->cal_AC5 = $this->_device.readU16BE(BMP085_CAL_AC5);   # UINT16
        $this->cal_AC6 = $this->_device.readU16BE(BMP085_CAL_AC6);   # UINT16
        $this->cal_B1 = $this->_device.readS16BE(BMP085_CAL_B1);     # INT16
        $this->cal_B2 = $this->_device.readS16BE(BMP085_CAL_B2);     # INT16
        $this->cal_MB = $this->_device.readS16BE(BMP085_CAL_MB);     # INT16
        $this->cal_MC = $this->_device.readS16BE(BMP085_CAL_MC);     # INT16
        $this->cal_MD = $this->_device.readS16BE(BMP085_CAL_MD);     # INT16
        $this->_logger.debug('AC1 = {0:6d}'.format($this->cal_AC1));
        $this->_logger.debug('AC2 = {0:6d}'.format($this->cal_AC2));
        $this->_logger.debug('AC3 = {0:6d}'.format($this->cal_AC3));
        $this->_logger.debug('AC4 = {0:6d}'.format($this->cal_AC4));
        $this->_logger.debug('AC5 = {0:6d}'.format($this->cal_AC5));
        $this->_logger.debug('AC6 = {0:6d}'.format($this->cal_AC6));
        $this->_logger.debug('B1 = {0:6d}'.format($this->cal_B1));
        $this->_logger.debug('B2 = {0:6d}'.format($this->cal_B2));
        $this->_logger.debug('MB = {0:6d}'.format($this->cal_MB));
        $this->_logger.debug('MC = {0:6d}'.format($this->cal_MC));
        $this->_logger.debug('MD = {0:6d}'.format($this->cal_MD));
    }
    function loadDatasheetCalibration(){
        # Set calibration from values in the datasheet example.  Useful for debugging the
        # temp and pressure calculation accuracy.
        $this->cal_AC1 = 408;
        $this->cal_AC2 = -72;
        $this->cal_AC3 = -14383;
        $this->cal_AC4 = 32741;
        $this->cal_AC5 = 32757;
        $this->cal_AC6 = 23153;
        $this->cal_B1 = 6190;
        $this->cal_B2 = 4;
        $this->cal_MB = -32767;
        $this->cal_MC = -8711;
        $this->cal_MD = 2868;
    }
    function readRawTemp(){
        #Reads the raw (uncompensated) temperature from the sensor
        $this->_device.write8(BMP085_CONTROL, BMP085_READTEMPCMD);
        time.sleep(0.005);  # Wait 5ms
        raw = $this->_device.readU16BE(BMP085_TEMPDATA);
        $this->_logger.debug('Raw temp 0x{0:X} ({1})'.format(raw & 0xFFFF, raw));
        return raw;
    }

    function readRawPressure(){
        #Reads the raw (uncompensated) pressure level from the sensor.
        $this->_device.write8(BMP085_CONTROL, BMP085_READPRESSURECMD + ($this->_mode << 6))
        if ($this->_mode == BMP085_ULTRALOWPOWER){
            time.sleep(0.005);
        } elseif ($this->_mode == BMP085_HIGHRES){
            time.sleep(0.014);

        } elseif ($this->_mode == BMP085_ULTRAHIGHRES){
            time.sleep(0.026);
        } else {
            time.sleep(0.008);
        }
        msb = $this->_device.readU8(BMP085_PRESSUREDATA);
        lsb = $this->_device.readU8(BMP085_PRESSUREDATA+1);
        xlsb = $this->_device.readU8(BMP085_PRESSUREDATA+2);
        raw = ((msb << 16) + (lsb << 8) + xlsb) >> (8 - $this->_mode);
        $this->_logger.debug('Raw pressure 0x{0:04X} ({1})'.format(raw & 0xFFFF, raw));
        return raw;
        }

    function readTemperature(){
        # Gets the compensated temperature in degrees celsius.
        UT = $this->read_raw_temp();
        # Datasheet value for debugging:
        #UT = 27898
        # Calculations below are taken straight from section 3.5 of the datasheet.
        X1 = ((UT - $this->cal_AC6) * $this->cal_AC5) >> 15;
        X2 = intdiv(($this->cal_MC << 11) , (X1 + $this->cal_MD));
        B5 = X1 + X2;
        temp = ((B5 + 8) >> 4) / 10.0;
        $this->_logger.debug('Calibrated temperature {0} C'.format(temp));
        return temp;
        }

    function readPressure(){
        # Gets the compensated pressure in Pascals.
        UT = $this->read_raw_temp();
        UP = $this->read_raw_pressure();
        # Datasheet values for debugging:
        #UT = 27898
        #UP = 23843
        # Calculations below are taken straight from section 3.5 of the datasheet.
        # Calculate true temperature coefficient B5.
        X1 = ((UT - $this->cal_AC6) * $this->cal_AC5) >> 15;
        X2 = intdiv(($this->cal_MC << 11) , (X1 + $this->cal_MD));
        B5 = X1 + X2;
        $this->_logger.debug('B5 = {0}'.format(B5));
        # Pressure Calculations
        B6 = B5 - 4000;
        $this->_logger.debug('B6 = {0}'.format(B6));
        X1 = ($this->cal_B2 * (B6 * B6) >> 12) >> 11;
        X2 = ($this->cal_AC2 * B6) >> 11;
        X3 = X1 + X2;
        B3 = intdiv(((($this->cal_AC1 * 4 + X3) << $this->_mode) + 2) , 4);
        $this->_logger.debug('B3 = {0}'.format(B3));
        X1 = ($this->cal_AC3 * B6) >> 13;
        X2 = ($this->cal_B1 * ((B6 * B6) >> 12)) >> 16;
        X3 = ((X1 + X2) + 2) >> 2;
        B4 = ($this->cal_AC4 * (X3 + 32768)) >> 15;
        $this->_logger.debug('B4 = {0}'.format(B4));
        B7 = (UP - B3) * (50000 >> $this->_mode);
        $this->_logger.debug('B7 = {0}'.format(B7));
        if (B7 < 0x80000000){
            p = (B7 * 2) / B4;
        }else{
            p = intdiv(B7,B4) * 2;
        }
        X1 = (p >> 8) * (p >> 8);
        X1 = (X1 * 3038) >> 16;
        X2 = (-7357 * p) >> 16;
        p = p + ((X1 + X2 + 3791) >> 4);
        $this->_logger.debug('Pressure {0} Pa'.format(p));
        return p;
    }
    function readAltitude($sealevel_pa=101325.0){
        #Calculates the altitude in meters.
        # Calculation taken straight from section 3.6 of the datasheet.
        pressure = float($this->read_pressure());
        altitude = 44330.0 * (1.0 - pow(pressure / $sealevel_pa, (1.0/5.255)));
        $this->_logger.debug('Altitude {0} m'.format(altitude));
        return altitude;
    }
    function readSealevelPressure($altitude_m=0.0){
        #Calculates the pressure at sealevel when given a known altitude in meters. Returns a value in Pascals.
        pressure = float($this->read_pressure());
        p0 = pressure / pow(1.0 - $altitude_m/44330.0, 5.255);
        $this->_logger.debug('Sealevel pressure {0} Pa'.format(p0));
        return p0;
    }

}

?>